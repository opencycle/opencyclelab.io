/**
 * Vélib'
 * Paris
 */

let titleCase = (str) =>
{
  return str
    .toLowerCase()
    .split(' ')
    .map(function(word) {
       return word[0].toUpperCase() + word.substr(1);
    })
    .join(' ');
}

let jcdecaux = (city, name_shift) =>
{
  let service_promise = new Promise((resolve, reject) =>
  {
    let service = {
      name: 'Vélib\'',
      city: 'Paris',
      position: {
        lattitude: 45.7796600,
        longitude: 3.0862800
      },
      stations: {}
    }
    let api_key = '847da02326e5a149d5a1035a44dfb58151ac347f'
    let api = 'https://api.jcdecaux.com/vls/v1/stations?contract=' + city + '&apiKey=' + api_key

    var myRequest = new Request(api);

    fetch(myRequest).then(response =>
    {
      return response.json();
    })
    .then(data =>
    {
      for (let i = 0 ; i < data.length ; i++)
      {
        let element = data[i]
        let name = element['name'].slice(name_shift);

        let position = {
          lattitude: element.position.lat,
          longitude: element.position.lng
        }

        service.stations[name] = {
          position: position,
          bicycles: element.available_bikes,
          free_places: element.available_bike_stands,
        }
      }

      resolve(service);
    });
  });

  return service_promise
}

export default jcdecaux;
