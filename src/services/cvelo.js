/**
 * C.vélo
 * Clermont-Ferrand
 */

let service_promise = new Promise((resolve, reject) =>
{
  let service = {
    name: 'C.vélo',
    city: 'Clermont-Ferrand',
    position: {
      lattitude: 45.7796600,
      longitude: 3.0862800
    },
    stations: {}
  }
  let test = false

  let proxy = test ? "http://localhost:8080/" : "https://damp-peak-97426.herokuapp.com/"

  let api = "http://cli-velo-clermont.gir.fr/avcstations.xml"

  let header = {
    "Access-Control-Allow-Headers": "X-Requested-With",
    "X-Requested-With": "XMLHttpRequest"
  }

  $.ajax({
      type: "GET",
      headers: header,
      url: proxy + api
  }).done(xmlDoc =>
  {
    for (let i = 0 ; i < 40 ; i++)
    {
      let element = xmlDoc.getElementsByTagName("si")[i];
      let name = element.getAttribute("na").slice(4);

      let position = {
        lattitude: parseFloat(element.getAttribute("la")),
        longitude: parseFloat(element.getAttribute("lg"))
      }

      service.stations[name] = {
        position: position,
        bicycles: element.getAttribute("av"),
        free_places: element.getAttribute("fr"),
      }
    }

    resolve(service);
  });
});

export default service_promise;
