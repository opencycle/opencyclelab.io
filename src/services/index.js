import cvelo from './cvelo'
import jcdecaux from './jcdecaux'

let services = {
  clermont: cvelo,
  lyon: jcdecaux('lyon', 8),
  paris: jcdecaux('paris', 8),
  mulhouse: jcdecaux('mulhouse', 5),
  besancon: jcdecaux('besancon', 5),
  marseille: jcdecaux('marseille', 5),
  toulouse: jcdecaux('toulouse', 8),
  rouen: jcdecaux('rouen', 4),
  amiens: jcdecaux('amiens', 4),
  nantes: jcdecaux('nantes', 6),
  nancy: jcdecaux('nancy', 8),
  cergy: jcdecaux('cergy-pontoise', 16),
  creteil: jcdecaux('creteil', 5),
}

export default services
