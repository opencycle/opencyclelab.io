import { nearestStation as _nearestStation } from './geolocation'
import { saveFavorite, removeFavorite } from './local_storage'

let setStation = (state, station_name) =>
{
  let station_info = state.stations[station_name]
  let is_favorite = state.favorite.items.indexOf(station_name) !== -1

  if (station_info)
  {
    let station = {
      name: station_name,
      bicycles: station_info.bicycles,
      free_places: station_info.free_places,
      is_favorite: is_favorite
    }

    return station
  }
  else
  {
    return null
  }
}

let selectStation = (state) =>
{
  let station_name = ''

  if (state.favorite.items.length > 0)
  {
    station_name = state.favorite.items[0]
  }
  else
  {
    let station_keys = Object.keys(state.stations)
    if (station_keys.length > 0)
    {
      station_name = station_keys[0]
    }
  }

  return station_name
}

let nearestStation = (state) =>
{
  let station_name = '';
  if (state.user_position)
  {
    station_name = _nearestStation(state.user_position.lattitude, state.user_position.longitude, state.stations)
  }
  return station_name
}

const mutations = {
  CHANGE_STATION(state, station_name) {
    state.current_station = setStation(state, station_name)
    state.favorite.index = -1
  },
  CHANGE_FAVORITE(state, favorite_index) {
    let station_name = '';
    state.favorite.index = favorite_index
    if (favorite_index == 0)
    {
      station_name = nearestStation(state)
    }
    else
    {
      station_name = state.favorite.items[favorite_index - 1]
    }
    state.current_station = setStation(state, station_name)
  },
  TOGGLE_STATION(state, station_name) {
    let index = state.favorite.items.indexOf(station_name)

    if (index !== -1)
    {
      state.favorite.items.splice(index, 1)
      removeFavorite(station_name)
    }
    else
    {
      state.favorite.items.push(station_name)
      state.favorite.items.sort()
      saveFavorite(station_name)
    }

    state.favorite.index = -1
    state.current_station = setStation(state, station_name)
  },
  NEXT_STATION(state) {
    if (state.favorite.index < state.favorite.items.length)
    {
      let station_name = ''

      if (state.favorite.index === -1 && !state.user_position)
      {
        state.favorite.index = 1
      }
      else
      {
        state.favorite.index += 1
      }

      if (state.favorite.index > 0)
      {
        station_name = state.favorite.items[state.favorite.index - 1];
      }
      else
      {
        station_name = nearestStation(state)
      }

      state.current_station = setStation(state, station_name)
    }
  },
  PREVIOUS_STATION(state) {
    if ((state.user_position && state.favorite.index > 0) || (!state.user_position && state.favorite.index > 1))
    {
      let station_name = ''
      state.favorite.index -= 1

      if (state.favorite.index > 0)
      {
        station_name = state.favorite.items[state.favorite.index - 1];
      }
      else
      {
        station_name = nearestStation(state)
      }

      state.current_station = setStation(state, station_name)
    }
  },
  NEW_BICYCLE_SERVICE(state, service) {
    let station_name = ''
    let favorite_index = -1

    state.stations = Object.assign({}, state.stations, service.stations);

    if (state.user_position)
    {
      station_name = nearestStation(state)
      if (station_name)
      {
        favorite_index = 0
      }
      else
      {
        station_name = selectStation(state)
      }
    }
    else
    {
      station_name = selectStation(state)
    }

    state.favorite.index = favorite_index
    if (station_name && station_name !== '') state.current_station = setStation(state, station_name)
  },
  USER_POSITION(state, user_position)
  {
    let station_name = ''
    let favorite_index = -1
    state.user_position = user_position

    station_name = nearestStation(state)
    if (station_name)
    {
      favorite_index = 0
    }
    else
    {
      station_name = selectStation(state)
    }

    state.favorite.index = favorite_index
    if (station_name && station_name !== '') state.current_station = setStation(state, station_name)
  },
  NO_USER_POSITION(state)
  {
    let station_name = selectStation(state)
    state.current_station = setStation(state, station_name)
    state.favorite.index = 1
  },
  SET_FAVORITES(state, favorites) {
    state.favorite.items = favorites
  }
}

export { mutations }
