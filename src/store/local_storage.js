import localforage from 'localforage'
require('localforage-startswith')

const OPENCYCLE_FAVORITE = 'OPENCYCLE-FAVORITE-';

const fetchFavorites = () =>
{
  return localforage.startsWith(OPENCYCLE_FAVORITE).then(res =>
  {
    return res;
  })
}

const saveFavorite = (favorite) =>
{
  return localforage.setItem(
    OPENCYCLE_FAVORITE + favorite,
    favorite
  ).then(value =>
  {
    return value
  }).catch(err =>
  {
    console.log('oops, we lost your favorites.')
  })
}

const removeFavorite = (favorite) =>
{
  return localforage.removeItem(
    OPENCYCLE_FAVORITE + favorite
  ).then(() =>
  {
    return true
  }).catch(err =>
  {
    return false
  })
}

export { fetchFavorites, saveFavorite, removeFavorite };
